import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
import sys

sys.path.append("/opt/apps/sourcesdir/arsel/ar-git-head/")
import ar

import matplotlib.pyplot as plt

# plot fonts
rc('font',family='serif')
rc('text', usetex=True)


resPath="../two-stream/"

Nplist = [256, 1024, 4096, 16384];
Dlist = {} # data set dict, to be indexed by elements of Nplist
Slist = {} # stats (i.e., arsel output) dict, to be indexed by elements of Nplist

omegap = 0.178E+11 # s^{-1}

dt = 1.401 # picoseconds
dt_nd = dt*1e-12*omegap # dt*omegap

plt.figure()
for Np in Nplist:
    Np_str = "{0:05d}".format(Np)
    Dlist[Np] = np.loadtxt(resPath+"dim_potenergy_vst_Np{0:05d}.dat".format(Np))

    D = Dlist[Np]

    # D[:,0] is time in nanoseconds.  So, mult by 1e-9 to get sec and
    # then mult by omegap to non-dimensionalize
    t_nd = D[:,0]*1e-9*omegap

    # Plot
    plt.semilogy(t_nd, D[:,1]/D[0,1], label='$N_p = {0:d}$'.format(Np))


# linear theory
gam = 0.5
Elin0 = 1e-2
tlin = np.linspace(0,20,129)
Elin = Elin0*np.exp(2*gam*tlin)
plt.semilogy(tlin, Elin, 'k--', label='Linear theory ($\gamma = 0.5$)')
    
plt.xlabel(r'$t \omega_p$', fontsize=16)
plt.ylabel(r'$E^2(t)/E^2(0)$', fontsize=16)
plt.ylim(1e-3, 1e5)
plt.grid()
plt.title(r"$\Delta t \omega_p = {0:.2f}, \Delta x / \lambda_D = {1:.2f}$".format(dt_nd, 0.25), fontsize=16)
plt.legend()
plt.savefig('potenergy.pdf', bbox_inches='tight')

plt.xlim(0, 50)
plt.savefig('potenergy_early.pdf', bbox_inches='tight')

# find start time (corresponding to t_nd = 200)
D = Dlist[256]
t_nd = D[:,0]*1e-9*omegap
ib = np.argmin(np.abs(t_nd - 200.0))

D = Dlist[Nplist[-1]] # in case the last one didn't finish
ie = len(D[:,0])

print "{0:8s}\t{1:16s}\t{1:16s}".format("Np", "mu", "mu_sigma")
for Np in Nplist:
    D = Dlist[Np]
    S = ar.arsel(D[ib:ie,1], True, True, 'CIC', 0, 512)
    Slist[Np] = S

    print "{0:8d}\t{1:4.8e}\t{2:4.8e}".format(Np, S.mu[0], S.mu_sigma[0])

