#!/bin/bash

MPIRUN=mpirun
NP=4
EDIPIC=../src/edipic
LOGFILE0=edipic_Np00256.log
LOGFILE1=edipic_Np01024.log
LOGFILE2=edipic_Np04096.log
LOGFILE3=edipic_Np16384.log
LOGFILE4=edipic_Np65536.log

INPUT0=ssc_initial_Np256.dat
INPUT=ssc_initial.dat

CURDIR=$PWD
RUNDIR=../two-stream/

cd $RUNDIR

## start with 256 particles per cell
#cp $INPUT0 $INPUT
#$MPIRUN -np $NP $EDIPIC >& $LOGFILE0
#mv dim_potenergy_vst.dat dim_potenergy_vst_Np00256.dat
#
## 1024
#sed -i 's/ 256 #Np/1024 #Np/' $INPUT
#$MPIRUN -np $NP $EDIPIC >& $LOGFILE1
#mv dim_potenergy_vst.dat dim_potenergy_vst_Np01024.dat
#
## 4096
#sed -i 's/1024 #Np/4096 #Np/' $INPUT
#$MPIRUN -np $NP $EDIPIC >& $LOGFILE2
#mv dim_potenergy_vst.dat dim_potenergy_vst_Np04096.dat
#
## 16384
#sed -i 's/ 4096 #Np/16384 #Np/' $INPUT
#$MPIRUN -np $NP $EDIPIC >& $LOGFILE3
#mv dim_potenergy_vst.dat dim_potenergy_vst_Np16384.dat

# 65536
sed -i 's/16384 #Np/65536 #Np/' $INPUT
$MPIRUN -np $NP $EDIPIC >& $LOGFILE4
mv dim_potenergy_vst.dat dim_potenergy_vst_Np65536.dat

cd $CURDIR
